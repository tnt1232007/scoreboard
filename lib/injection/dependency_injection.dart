import '../data/game_data.dart';
import '../data/mock/data_mock.dart';

class Injector {
  static final Injector _singleton = new Injector._internal();

  factory Injector() {
    return _singleton;
  }

  Injector._internal();

  IGameRepository get gameRepository {
    return new MockGameRepository();
  }
}
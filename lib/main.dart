import 'package:flutter/material.dart';

import 'module/home/home_page.dart';
import 'module/settings/setting_page.dart';

void main() => runApp(new MainApp());

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Scoreboard',
      home: new HomePage(),
      routes: <String, WidgetBuilder>{
        SettingPage.routeName: (context) => new SettingPage(),
      },
    );
  }
}

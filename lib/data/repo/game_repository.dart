import '../game_data.dart';
import 'repository.dart';

final String columnName = "name";
final String columnState = "state";
final String columnColor = "color";
final String columnStart = "start";
final String columnEnd = "end";
final String columnRounds = "rounds";

class GameSchema extends ISchema<Game> {
  @override
  String get tableName => "games";

  @override
  List<String> get columns => <String>[
        columnId,
        columnName,
        columnState,
        columnColor,
        columnStart,
        columnEnd,
        columnRounds,
        columnCreatedAt,
      ];

  @override
  String get createScript => '''
    CREATE TABLE $tableName (
        $columnId        INTEGER PRIMARY KEY AUTOINCREMENT,
        $columnName      TEXT     NOT NULL,
        $columnState     INTEGER  NOT NULL,
        $columnColor     INTEGER  NOT NULL,
        $columnStart     INTEGER  NOT NULL,
        $columnEnd       INTEGER  NOT NULL,
        $columnRounds    INTEGER  NOT NULL,
        $columnCreatedAt DATETIME NOT NULL
    )
  ''';

  @override
  Game fromMap(Map map) {
    var g = new Game();
    g.id = map[columnId];
    g.name = map[columnName];
    g.state = map[columnState];
    g.color = map[columnColor];
    g.start = map[columnStart];
    g.end = map[columnEnd];
    g.createdAt = map[columnCreatedAt];
    return g;
  }

  @override
  Map toMap(Game entity) {
    Map map = {
      columnName: entity.name,
      columnState: entity.state,
      columnColor: entity.color,
      columnStart: entity.start,
      columnEnd: entity.end,
      columnCreatedAt: entity.createdAt,
    };
    if (entity.id != null) map[columnId] = entity.id;
    return map;
  }
}

class GameRepository extends IRepository<Game> implements IGameRepository {
  GameRepository() : super(new GameSchema());
}

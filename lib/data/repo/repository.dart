import 'dart:async';

import '../base/entity.dart';

// TODO: sqflite not working, finding new sqlite package
// import 'package:sqflite/sqflite.dart';

final String columnId = "_id";
final String columnCreatedAt = "createdAt";

abstract class ISchema<T extends IEntity> {
  String tableName;
  String createScript;
  List<String> columns;

  T fromMap(Map map);
  Map toMap(T entity);
}


abstract class IRepository<T extends IEntity> {
  dynamic _context;
  ISchema<T> _schema;

  IRepository(this._schema);

  Future open(String path) async {
    // _context = await openDatabase(path, version: 1, onCreate: (dynamic db, int version) async {
    //   await db.execute(_schema.createScript);
    // });
  }

  Future<T> insert(T entity) async {
    entity.id = await _context.insert(_schema.tableName, _schema.toMap(entity));
    return entity;
  }

  Future<List<T>> fetch() async {
    List<Map> maps = await _context.query(
      _schema.tableName,
      columns: _schema.columns
    );
    return maps.map((map) => _schema.fromMap(map));
  }

  Future<T> get(int id) async {
    List<Map> map = await _context.query(
      _schema.tableName,
      columns: _schema.columns,
      where: "$columnId = ?",
      whereArgs: [id],
    );
    return map.length > 0 ? _schema.fromMap(map.first) : null;
  }

  Future<int> delete(int id) async {
    return await _context.delete(_schema.tableName, where: "$columnId = ?", whereArgs: [id]);
  }

  Future<int> update(T entity) async {
    return await _context.update(
      _schema.tableName,
      _schema.toMap(entity),
      where: "$columnId = ?",
      whereArgs: [entity.id],
    );
  }

  Future close() async => _context.close();
}

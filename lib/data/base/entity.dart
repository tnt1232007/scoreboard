abstract class IEntity {
  int id;
  DateTime createdAt = new DateTime.now();

  IEntity({this.id, this.createdAt});
}
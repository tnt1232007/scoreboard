import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../common/game_state.dart';
import '../../common/random.dart';
import '../game_data.dart';
import '../player_data.dart';

class MockPlayerRepository {
  static const _kRandomUserUrl = 'http://api.randomuser.me/?nat=us&results=';
  final JsonDecoder _decoder = new JsonDecoder();

  Future<List<Player>> fetch(int count) {
    return http.get("$_kRandomUserUrl$count").then((http.Response response) {
      final String jsonBody = response.body;
      final statusCode = response.statusCode;

      if (statusCode < 200 || statusCode >= 300 || jsonBody == null)
        throw new Exception("Error [StatusCode:$statusCode]");

      final List users = _decoder.convert(jsonBody)['results'];
      var count = 1;
      return users.map((user) {
        var player = new Player(
          "${user['name']['first'][0].toString().toUpperCase()}${user['name']['first'].toString().substring(1)}",
          Randomizer.pick(Colors.primaries).value,
        );
        player.id = count++;
        return player;
      }).toList();
    });
  }
}

class MockGameRepository implements IGameRepository {
  final _playerRespository = new MockPlayerRepository();

  @override
  Future<List<Game>> fetch() async {
    var games = new List<Game>();
    for (var i = 0; i < 4; i++) {
      var players = await _playerRespository.fetch(Randomizer.$int(min: 2, max: 8));
      var rounds = Randomizer.$int(max: 50);
      for (var player in players) {
        for (var j = 0; j < rounds; j++) {
          var score = Randomizer.$int(min: -3, max: 3);
          player.scores.add(score);
          player.totalScore += score;
        }
      }

      var sortPlayers = new List<Player>.from(players);
      sortPlayers.sort((a, b) => b.totalScore - a.totalScore);
      for (var j = 0; j < sortPlayers.length; j++) {
        sortPlayers[j].rank = j + 1;
      }

      var game = new Game.fromPlayers(players);
      game.id = i;
      game.rounds = rounds;
      game.state = GameState.Active;
      game.color = Randomizer.pick(Colors.primaries).value;
      game.createdAt = new DateTime.now();
      games.add(game);
    }

    var completer = new Completer<List<Game>>();
    completer.complete(games);
    return completer.future;
  }
}

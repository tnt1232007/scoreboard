import 'dart:async';

import 'base/entity.dart';

class Player extends IEntity {
  int id;
  String name;
  int color;

  final String initial;
  List<int> scores = [];
  int totalScore = 0;
  int rank = 1;

  Player(this.name, this.color) : initial = name.isEmpty ? '' : name[0];
}

abstract class IPlayerRepository {
  Future<List<Player>> fetch();
}

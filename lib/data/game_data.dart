import 'dart:async';

import '../common/game_state.dart';
import 'base/entity.dart';
import 'player_data.dart';

class Game extends IEntity {
  String name;
  GameState state;
  int color;
  int start = 0;
  int end = 100;
  int rounds = 0;
  List<Player> players = [];

  Game();
  Game.fromPlayers(this.players, {this.name});

  String initial() => name == null || name.isEmpty
      ? "${players.length}${players.length > 0 ? players[0].initial : ''}"
      : name.split(' ').where((s) => s.length > 0).map((s) => s[0]).join('').toUpperCase();
}

abstract class IGameRepository {
  Future<List<Game>> fetch();
}

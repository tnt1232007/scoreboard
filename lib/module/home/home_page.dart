import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../common/constants.dart';
import '../games/game_list_page.dart';

class HomePage extends StatelessWidget {
  final _googleSignIn = new GoogleSignIn();
  final _analytics = new FirebaseAnalytics();
  final _firebaseAuth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    _ensureLogin(context);
    final activeColor = Constants.ActiveColor;
    return new Container(
      color: Colors.white,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          new Column(
            children: <Widget>[
              new Icon(
                Icons.assessment,
                color: activeColor,
                size: 120.0,
              ),
              new Text(
                "Scoreboard",
                style: Theme.of(context).textTheme.display2.copyWith(color: activeColor),
              ),
            ],
          ),
          new Column(
            children: <Widget>[
              new CircularProgressIndicator(backgroundColor: activeColor),
            ],
          )
        ],
      ),
    );
  }

  Future _ensureLogin(BuildContext context) async {
    try {
      GoogleSignInAccount user = _googleSignIn.currentUser;
      if (user == null) {
        user = await _googleSignIn.signInSilently();
      }
      if (user == null) {
        await _googleSignIn.signIn();
        _analytics.logLogin();
      }

      var currentUser = await _firebaseAuth.currentUser();
      if (currentUser == null) {
        var credentials = await _googleSignIn.currentUser.authentication;
        if (credentials != null) {
          currentUser = await _firebaseAuth.signInWithGoogle(
            idToken: credentials.idToken,
            accessToken: credentials.accessToken,
          );
        }
      }

      _start(context, currentUser);
    } catch (e) {
      _showError(context, e.toString());
    }
  }

  Future<Null> _showError(BuildContext context, String text) {
    return showDialog<Null>(
      context: context,
      barrierDismissible: false,
      child: new AlertDialog(
        title: new Text('Error'),
        content: new SingleChildScrollView(
          child: new ListBody(
            children: <Widget>[
              new Text('Something goes wrong'),
              new Text('Please try again'),
            ],
          ),
        ),
        actions: <Widget>[
          new FlatButton(
            child: new Text('Retry'),
            onPressed: () {
              Navigator.of(context).pop();
              _ensureLogin(context);
            },
          ),
          new FlatButton(
            child: new Text('Skip'),
            onPressed: () {
              Navigator.of(context).pop();
              _start(context, null);
            },
          ),
          new FlatButton(
            child: new Text('Exit'),
            onPressed: () => exit(0),
          ),
        ],
      ),
    );
  }

  void _start(BuildContext context, FirebaseUser user) {
    Navigator.of(context).pushReplacement(
          new MaterialPageRoute(
            builder: (context) => new GameListPage(user),
          ),
        );
  }
}

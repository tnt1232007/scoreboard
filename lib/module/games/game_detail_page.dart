import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../common/constants.dart';
import '../../data/game_data.dart';
import '../../data/player_data.dart';
import '../widgets/custom_circle_avatar.dart';

class GameDetailPage extends StatefulWidget {
  static const String routeName = '/game';
  final Game _game;

  GameDetailPage(this._game);

  @override
  GameDetailState createState() => new GameDetailState();
}

class GameDetailState extends State<GameDetailPage> {
  void _calcRounds() {
    setState(() => widget._game.rounds =
        widget._game.players.fold(0, (t, player) => player.scores.length > t ? player.scores.length : t));
  }

  void _undoLastRound() {
    setState(() {
      widget._game.players.forEach((player) {
        var lastScore = player.scores.removeLast();
        player.totalScore -= lastScore;
      });
    });
    _calcRounds();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("${widget._game.rounds} rounds"),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.undo),
            tooltip: 'Undo Last Round',
            onPressed: _undoLastRound,
          ),
        ],
      ),
      body: new PlayerList(widget._game.players, roundsChanged: _calcRounds),
    );
  }
}

class PlayerList extends StatefulWidget {
  final List<Player> _players;
  final VoidCallback roundsChanged;

  PlayerList(this._players, {@required this.roundsChanged});

  @override
  _PlayerListState createState() => new _PlayerListState();
}

class _PlayerListState extends State<PlayerList> {
  void _calcRanks() {
    setState(() => widget.roundsChanged());
  }

  @override
  Widget build(BuildContext context) {
    var sortPlayers = new List<Player>.from(widget._players);
    sortPlayers.sort((a, b) => b.totalScore - a.totalScore);
    sortPlayers.forEach((player) => player.rank = sortPlayers.indexOf(player) + 1);
    return new ListView.builder(
      itemCount: widget._players.length,
      itemBuilder: (context, i) => new PlayerItem(widget._players[i], scoreChanged: _calcRanks),
    );
  }
}

class PlayerItem extends StatefulWidget {
  final Player _player;
  final VoidCallback scoreChanged;

  PlayerItem(this._player, {@required this.scoreChanged}) : super(key: new Key(_player.id.toString()));

  @override
  _PlayerItemState createState() => new _PlayerItemState();
}

class _PlayerItemState extends State<PlayerItem> {
  void _addScore(int score) {
    setState(() {
      widget._player.scores.add(score);
      widget._player.totalScore += score;
      widget.scoreChanged();
    });
  }

  void _removeLastScore() {
    setState(() {
      var lastScore = widget._player.scores.removeLast();
      widget._player.totalScore -= lastScore;
      widget.scoreChanged();
    });
  }

  void _clearAllScore() {
    setState(() {
      widget._player.scores = [];
      widget._player.totalScore = 0;
      widget.scoreChanged();
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Card(
      child: new Column(
        children: <Widget>[
          new Row(
            children: <Widget>[
              new Padding(
                padding: new EdgeInsets.only(left: 15.0, top: 15.0, bottom: 15.0, right: 5.0),
                child: new CircleTextAvatar(text: widget._player.initial, color: widget._player.color),
              ),
              new Expanded(
                flex: 3,
                child: new PlayerInfo(
                  name: widget._player.name,
                  rank: widget._player.rank,
                  color: widget._player.color,
                ),
              ),
              new Expanded(
                flex: 2,
                child: new ScoreBar(widget._player.totalScore),
              ),
              new ScoreList(widget._player.scores),
            ],
          ),
          new Divider(
            height: 0.0,
            color: Colors.grey,
          ),
          new ButtonRow(
            widget._player,
            addScore: _addScore,
            removeLastScore: _removeLastScore,
            clearAllScore: _clearAllScore,
          )
        ],
      ),
    );
  }
}

class PlayerInfo extends StatelessWidget {
  final String name;
  final int rank;
  final int color;

  PlayerInfo({this.name, this.rank, this.color});

  @override
  Widget build(BuildContext context) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Text(
          name.isEmpty ? '' : name.substring(1),
          style: Theme.of(context).textTheme.title.copyWith(color: new Color(color)),
        ),
        new Text(
          "$rank${rank == 1 ? 'st' : rank == 2 ? 'nd' : 'th'} place",
          style: Theme.of(context).textTheme.caption,
        ),
      ],
    );
  }
}

class ScoreBar extends StatelessWidget {
  final int _score;
  final double _percent;
  final Color _color;

  ScoreBar(this._score)
      : _percent = (_score > 0 ? _score : -_score) / 100.0,
        _color = _score >= 0 ? Constants.PositiveColor : Constants.NegativeColor;

  @override
  Widget build(BuildContext context) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        new Text(
          _score.toString(),
          style: Theme.of(context).textTheme.headline.copyWith(color: _color),
        ),
        new LinearProgressIndicator(
          value: _percent,
          backgroundColor: _color.withAlpha(100),
          valueColor: new AlwaysStoppedAnimation<Color>(_color),
        ),
      ],
    );
  }
}

class ScoreList extends StatelessWidget {
  final List<int> _scores;

  ScoreList(this._scores);

  @override
  Widget build(BuildContext context) {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _buildScoreList(context, _scores).toList(),
    );
  }

  Iterable<Widget> _buildScoreList(BuildContext context, List<int> scores) sync* {
    for (var i = 0; i < 5; i++) {
      var round = i + scores.length - 5;
      var score = round >= 0 && scores.length > round ? scores[round] : null;
      var color = score == null || score >= 0 ? Constants.PositiveColor : Constants.NegativeColor;
      var signScore;
      if (score == null) {
        signScore = '';
      } else if (score > 0) {
        signScore = "${Constants.plus}$score";
      } else if (score < 0) {
        signScore = "${Constants.minus}${-score}";
      } else {
        signScore = '  0';
      }

      yield new Padding(
        padding: const EdgeInsets.symmetric(vertical: 1.0, horizontal: 15.0),
        child: new ConstrainedBox(
          constraints: new BoxConstraints(minWidth: 25.0),
          child: new Row(
            children: <Widget>[
              new Text(round >= 0 ? "${round + 1}. " : '', textScaleFactor: 0.7),
              new Text("$signScore", style: Theme.of(context).textTheme.caption.copyWith(color: color)),
            ],
          ),
        ),
      );
    }
  }
}

class ButtonRow extends StatefulWidget {
  final Player _player;
  final ValueSetter<int> addScore;
  final VoidCallback removeLastScore;
  final VoidCallback clearAllScore;

  ButtonRow(
    this._player, {
    @required this.addScore,
    @required this.removeLastScore,
    @required this.clearAllScore,
  });

  @override
  _ButtonRowState createState() => new _ButtonRowState();
}

class _ButtonRowState extends State<ButtonRow> {
  bool _negative = false;
  Color _color = Constants.PositiveColor;
  String _sign = Constants.plus;

  void _toggleNegative() {
    setState(() {
      _negative = !_negative;
      _color = _negative ? Constants.NegativeColor : Constants.PositiveColor;
      _sign = _negative ? Constants.minus : Constants.plus;
    });
  }

  void _addScore(int score) {
    widget.addScore(_negative ? -score : score);
  }

  @override
  Widget build(BuildContext context) {
    return new ButtonTheme(
      child: new Row(
        children: <Widget>[
          new FlatButton(
            child: const Text('CLEAR'),
            textColor: new Color(widget._player.color),
            onPressed: () => widget.clearAllScore(),
          ),
          new Expanded(
            child: new FlatButton(
              child: new Text(Constants.plusMinus),
              textColor: _color,
              padding: EdgeInsets.zero,
              onPressed: _toggleNegative,
            ),
          ),
          _buildScoreButton(0),
          _buildScoreButton(1),
          _buildScoreButton(2),
          _buildScoreButton(3),
          new FlatButton(
            child: const Text('UNDO'),
            textColor: new Color(widget._player.color),
            onPressed: () => widget.removeLastScore(),
          ),
        ],
      ),
      minWidth: 0.0,
    );
  }

  Widget _buildScoreButton(int score) {
    return new Expanded(
      child: new GestureDetector(
        onLongPress: () => _addScore(-score),
        child: new FlatButton(
          child: new Text('${score == 0 ? '' : _sign}$score'),
          textColor: _color,
          padding: EdgeInsets.zero,
          onPressed: () => _addScore(score),
        ),
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_color_picker/flutter_color_picker.dart';

import '../../common/random.dart';
import '../../data/game_data.dart';
import '../../data/player_data.dart';
import '../widgets/system_padding.dart';

class GameNewPage extends StatefulWidget {
  static const String routeName = '/game/new';

  @override
  _GameNewPageState createState() => new _GameNewPageState();
}

class _GameNewPageState extends State<GameNewPage> {
  final ScrollController _scrollController = new ScrollController();
  final TextEditingController _nameController = new TextEditingController();
  final TextEditingController _startController = new TextEditingController();
  final TextEditingController _endController = new TextEditingController();
  var _players = <Player>[
    new Player('Player1', Colors.red.value),
    new Player('Player2', Colors.blue.value),
  ];

  @override
  Widget build(BuildContext context) {
    var caption = Theme.of(context).textTheme.caption;
    var game = new Game.fromPlayers(_players);
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('New game'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('START'),
            textColor: Colors.white,
            onPressed: _saveGame,
          )
        ],
      ),
      body: new Form(
        child: new ListView(
          controller: _scrollController,
          children: <Widget>[
            _buildFormInput(
              'Name',
              controller: _nameController,
              hintText: game.initial(),
              textStyle: Theme.of(context).textTheme.display1,
            ),
            new Row(
              children: <Widget>[
                new Expanded(
                  child: _buildFormInput(
                    'Start',
                    controller: _startController,
                    initialValue: 0,
                    keyboardType: TextInputType.number,
                  ),
                ),
                new Expanded(
                  child: _buildFormInput(
                    'End',
                    controller: _endController,
                    initialValue: 100,
                    keyboardType: TextInputType.number,
                  ),
                )
              ],
            ),
            new Padding(
              padding: const EdgeInsets.all(16.0),
              child: new Text('Participants', style: caption),
            ),
          ]
            ..addAll(_players.map((player) => _buildPlayerTile(player)))
            ..add(
              new FlatButton.icon(
                label: new Text('ADD PLAYER'),
                icon: new Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: new Icon(Icons.add_circle),
                ),
                onPressed: _addPlayer,
              ),
            ),
        ),
      ),
    );
  }

  void _saveGame() {
    Navigator.of(context).pop(new Game.fromPlayers(_players, name: _nameController.text));
  }

  void _addPlayer() {
    var indexName = 1;
    while (true) {
      if (_players.map((player) => player.name).contains('Player$indexName'))
        indexName++;
      else
        break;
    }
    var player = new Player('Player$indexName', Randomizer.pick(Colors.primaries).value);
    setState(() => _players.add(player));
    SchedulerBinding.instance.addPostFrameCallback((_) {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    });
  }

  Future _editPlayer(Player player) async {
    final TextEditingController _controller = new TextEditingController();
    var newName = await showDialog<String>(
      context: context,
      child: new SystemPadding(
        child: new AlertDialog(
          title: const Text('Player name'),
          content: new TextFormField(
            controller: _controller,
            initialValue: player.name,
            autofocus: true,
            decoration: new InputDecoration(),
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text('CANCEL'),
              onPressed: () => Navigator.of(context).pop(),
            ),
            new FlatButton(
              child: new Text('OK'),
              onPressed: () => Navigator.of(context).pop(_controller.text),
            )
          ],
        ),
      ),
    );
    setState(() => player.name = newName ?? player.name);
  }

  void _removePlayer(Player player) {
    setState(() => _players.remove(player));
  }

  Widget _buildFormInput(
    String label, {
    TextEditingController controller,
    TextStyle textStyle,
    dynamic initialValue = '',
    String hintText = '',
    TextInputType keyboardType = TextInputType.text,
  }) {
    return new Padding(
      padding: new EdgeInsets.all(16.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(label, style: Theme.of(context).textTheme.caption),
          new TextFormField(
            controller: controller,
            decoration: new InputDecoration(hintText: hintText),
            style: textStyle ?? Theme.of(context).textTheme.body1,
            initialValue: initialValue.toString(),
            keyboardType: keyboardType,
          ),
        ],
      ),
    );
  }

  Widget _buildPlayerTile(Player player) {
    return new Dismissible(
      key: new Key("${_players.indexOf(player)}${player.name}"),
      onDismissed: (direction) => _removePlayer(player),
      background: new Container(color: new Color(player.color)),
      child: new ListTile(
        title: new Row(children: [
          new ColorTile(
            color: new Color(player.color),
            size: 30.0,
            rounded: true,
            onTap: () async {
              var color = await showDialog<Color>(
                context: context,
                child: new PrimaryColorPickerDialog(
                  // cannot compare const Color object with new Color(player.color)
                  selected: Colors.primaries.firstWhere((color) => color.value == player.color),
                ),
              );
              if (color != null) player.color = color.value;
            },
          ),
          new Padding(padding: new EdgeInsets.symmetric(horizontal: 5.0)),
          new Expanded(
            child: new GestureDetector(
              onTap: () => _editPlayer(player),
              child: new Text(player.name),
            ),
          ),
          new IconButton(icon: new Icon(Icons.edit), onPressed: () => _editPlayer(player)),
        ]),
      ),
    );
  }
}

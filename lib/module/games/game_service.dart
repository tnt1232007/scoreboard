import 'dart:async';

import '../../data/game_data.dart';
import '../../injection/dependency_injection.dart';

class GameService {
  IGameRepository _repository;

  GameService() {
    _repository = new Injector().gameRepository;
  }

  Future<List<Game>> loadGames() {
    return _repository.fetch();
  }
}

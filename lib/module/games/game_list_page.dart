import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:timeago/timeago.dart';

import '../../common/constants.dart';
import '../../common/game_state.dart';
import '../../data/game_data.dart';
import '../widgets/custom_circle_avatar.dart';
import '../widgets/navigation_drawer.dart';
import 'game_detail_page.dart';
import 'game_new_page.dart';
import 'game_service.dart';

class GameListPage extends StatefulWidget {
  final FirebaseUser _currentUser;
  final _analytics = new FirebaseAnalytics();
  final _service = new GameService();

  GameListPage(this._currentUser);

  @override
  GameListState createState() => new GameListState();
}

class GameListState extends State<GameListPage> {
  Map<Game, bool> _games;
  GameState _selectedState = GameState.Active;
  int _selectedCount = 0;
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    widget._analytics.logAppOpen();
    _isLoading = true;
    _loadGames();
  }

  @override
  Widget build(BuildContext context) {
    var selectedDrawerItem = DrawerItems[_selectedState];
    _selectedCount = _isLoading ? 0 : _games.values.where((selected) => selected).length;
    return new Scaffold(
      appBar: _buildAppBar(selectedDrawerItem),
      drawer: new NavigationDrawer(
        _selectedState,
        currentUser: widget._currentUser,
        onSelectDrawerItem: (state) => setState(() => _selectedState = state),
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        onPressed: _showGameNew,
      ),
      body: _isLoading
          ? new Center(child: new CircularProgressIndicator())
          : new ListView(children: _buildGameList(_selectedCount)),
    );
  }

  AppBar _buildAppBar(DrawerItem selectedDrawerItem) {
    if (_selectedCount > 0) {
      return new AppBar(
        leading: new IconButton(
          icon: const BackButtonIcon(),
          onPressed: () => _selectGames(false),
          tooltip: 'Unselect All',
        ),
        backgroundColor: Constants.deactiveColor,
        automaticallyImplyLeading: false,
        title: new Text("$_selectedCount/${_games.length}"),
        actions: DrawerItems.keys.map((state) => _buildAppBarButton(state)).toList(),
      );
    } else {
      return new AppBar(
        title: new Text('${selectedDrawerItem.title}'),
        backgroundColor: selectedDrawerItem.color,
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.select_all),
            onPressed: () => _selectGames(true),
            tooltip: 'Select All',
          ),
        ],
      );
    }
  }

  Widget _buildAppBarButton(GameState newState) {
    var drawerItem = DrawerItems[newState];
    return _selectedState == newState
        ? new Container()
        : new Builder(
            builder: (context) {
              return new IconButton(
                icon: new Icon(drawerItem.icon),
                tooltip: 'Move to ${DrawerItems[newState].title}',
                onPressed: () {
                  var selectedState = _selectedState;
                  var selectedGames = _games.keys.where((o) => _games[o]).toList();
                  Scaffold.of(context).removeCurrentSnackBar();
                  Scaffold.of(context).showSnackBar(new SnackBar(
                        duration: new Duration(seconds: 5),
                        content: new Text(
                            "$_selectedCount game${_selectedCount > 0 ? 's' : ''} moved to ${DrawerItems[newState].title}"),
                        action: new SnackBarAction(
                          label: 'UNDO',
                          onPressed: () => _changeSelectedGameState(selectedGames, selectedState),
                        ),
                      ));
                  _changeSelectedGameState(selectedGames, newState);
                },
              );
            },
          );
  }

  List<GameItem> _buildGameList(int selectedCount) {
    return _games.keys
        .where((game) => game.state == _selectedState)
        .map((game) => new GameItem(
              game,
              _games[game],
              selectedCount,
              onTap: () => _showGameDetail(game),
              onSelected: (selected) => _selectGame(game, selected),
              onDismissed: (state) => _changeGameState(game, state),
            ))
        .toList();
  }

  void _loadGames() async {
    var games = await widget._service.loadGames();
    setState(() {
      _games = new Map.fromIterable(games, key: (game) => game, value: (game) => false);
      _isLoading = false;
    });
  }

  void _selectGames(bool selected) {
    setState(() => _games.forEach((game, _) => _games[game] = selected));
  }

  void _changeSelectedGameState(List<Game> selectedGames, GameState newState) {
    setState(() {
      selectedGames.forEach((game) {
        game.state = newState;
        _games[game] = false;
      });
    });
  }

  Future _showGameNew() async {
    var game = await Navigator.of(context).push(new MaterialPageRoute<Game>(builder: (context) => new GameNewPage()));
    if (game == null) return;
    setState(() => _games[game] = false);
  }

  void _showGameDetail(Game game) {
    Navigator.of(context).push(new MaterialPageRoute(builder: (context) => new GameDetailPage(game)));
  }

  void _selectGame(Game game, bool selected) {
    setState(() => _games[game] = selected);
  }

  void _changeGameState(Game game, GameState newState) {
    setState(() => game.state = newState);
  }
}

class GameItem extends StatefulWidget {
  final Game _game;
  final bool _isSelected;
  final int _selectedCount;
  final GestureTapCallback onTap;
  final ValueSetter<GameState> onDismissed;
  final ValueSetter<bool> onSelected;

  GameItem(this._game, this._isSelected, this._selectedCount,
      {@required this.onTap, @required this.onSelected, @required this.onDismissed})
      : super(key: new Key(_game.id.toString()));

  @override
  GameItemState createState() => new GameItemState();
}

class GameItemState extends State<GameItem> {
  bool _isSelected;

  @override
  Widget build(BuildContext context) {
    _isSelected = widget._isSelected;

    GameState _startToEndState;
    GameState _endToStartState;
    switch (widget._game.state) {
      case GameState.Active:
        _startToEndState = GameState.Archive;
        _endToStartState = GameState.Delete;
        break;
      case GameState.Archive:
        _startToEndState = GameState.Delete;
        _endToStartState = GameState.Active;
        break;
      case GameState.Delete:
        _startToEndState = GameState.Active;
        _endToStartState = GameState.Archive;
        break;
      default:
        throw new ArgumentError(widget._game.state);
    }

    return new Dismissible(
      key: new Key(widget._game.id.toString()),
      background: _buildLeaveBehindItem(_startToEndState, DismissDirection.startToEnd),
      secondaryBackground: _buildLeaveBehindItem(_endToStartState, DismissDirection.endToStart),
      onDismissed: (direction) {
        final GameState oldState = widget._game.state;
        widget.onDismissed(direction == DismissDirection.startToEnd ? _startToEndState : _endToStartState);
        Scaffold.of(context).removeCurrentSnackBar();
        Scaffold.of(context).showSnackBar(new SnackBar(
              duration: new Duration(seconds: 5),
              content: new Text("${widget._game.initial} moved to ${DrawerItems[widget._game.state].title}"),
              action: new SnackBarAction(label: 'UNDO', onPressed: () => widget.onDismissed(oldState)),
            ));
      },
      child: new InkWell(
        onTap: () {
          if (widget._selectedCount > 0)
            _toggleSelected();
          else
            widget.onTap();
        },
        onLongPress: () {
          _toggleSelected();
          HapticFeedback.vibrate();
        },
        child: new Container(
          color: _isSelected ? Constants.selectedColor : Colors.transparent,
          padding: const EdgeInsets.all(15.0),
          child: new Row(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.only(right: 15.0),
                child: new GestureDetector(
                  onTap: _toggleSelected,
                  child: _isSelected
                      ? new CircleIconAvatar(icon: Icons.check)
                      : new CircleTextAvatar(text: widget._game.initial(), color: widget._game.color),
                ),
              ),
              new Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Text(
                      widget._game.players.map((o) => o.name).join(', '),
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                    new Row(children: <Widget>[
                      new Expanded(
                        child: new Text(
                          '${widget._game.rounds} rounds',
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                      new Text(
                        timeAgo(widget._game.createdAt),
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ]),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static Widget _buildLeaveBehindItem(GameState state, DismissDirection direction) {
    return new Container(
      color: DrawerItems[state].color,
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: new Row(
        mainAxisAlignment: direction == DismissDirection.startToEnd ? MainAxisAlignment.start : MainAxisAlignment.end,
        children: <Widget>[
          new Icon(
            DrawerItems[state].icon,
            color: Colors.white,
          )
        ],
      ),
    );
  }

  void _toggleSelected() {
    setState(() => _isSelected = !_isSelected);
    widget.onSelected(_isSelected);
  }
}

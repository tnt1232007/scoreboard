import 'package:flutter/material.dart';

class SettingPage extends StatelessWidget {
  static const String routeName = '/settings';

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text('Settings')),
      body: new Center(
        child: new FlatButton(
          child: new Text('POP'),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
    );
  }
}

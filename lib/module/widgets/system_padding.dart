import 'package:flutter/material.dart';

// HACK: Temporary fix https://stackoverflow.com/questions/46841637/show-a-text-field-dialog-without-being-covered-by-keyboard
class SystemPadding extends StatelessWidget {
  final Widget child;

  SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
      padding: mediaQuery.padding,
      duration: const Duration(milliseconds: 300),
      child: child,
    );
  }
}

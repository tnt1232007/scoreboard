import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../common/constants.dart';
import '../../common/game_state.dart';
import '../settings/setting_page.dart';
import 'fade_in_image_provider.dart';
import 'shadow_text.dart';

class DrawerItem {
  final String title;
  final IconData icon;
  final Color color;
  const DrawerItem(this.title, this.icon, this.color);
}

const DrawerItems = const {
  GameState.Active: const DrawerItem("Games", Icons.assessment, Constants.ActiveColor),
  GameState.Archive: const DrawerItem("Archives", Icons.archive, Constants.ArchiveColor),
  GameState.Delete: const DrawerItem("Bin", Icons.delete, Constants.DeleteColor),
};

class NavigationDrawer extends StatefulWidget {
  final GameState _selectedState;
  final FirebaseUser currentUser;
  final ValueChanged<GameState> onSelectDrawerItem;

  NavigationDrawer(this._selectedState, {this.currentUser, @required this.onSelectDrawerItem});

  @override
  NavigationDrawerState createState() {
    return new NavigationDrawerState();
  }
}

class NavigationDrawerState extends State<NavigationDrawer> {
  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: new ListView(
        children: <Widget>[_buildDrawerHeader()]
          ..addAll(DrawerItems.keys.map((state) => _buildDrawerItem(state)))
          ..addAll(<Widget>[
            new Divider(height: 0.0, color: Colors.grey),
            new ListTile(
              leading: new Icon(Icons.settings),
              title: new Text('Settings'),
              onTap: () => Navigator.of(context)
                ..pop()
                ..pushNamed(SettingPage.routeName),
            ),
          ]),
      ),
    );
  }

  Widget _buildDrawerHeader() {
    return new UserAccountsDrawerHeader(
      accountName: new ShadowText(widget.currentUser?.displayName?.toUpperCase() ?? ''),
      accountEmail: new ShadowText(widget.currentUser?.email ?? ''),
      currentAccountPicture: new CircleAvatar(
        backgroundColor: Colors.transparent,
        backgroundImage: new FadeInImageProvider(widget.currentUser?.photoUrl),
      ),
      decoration: new BoxDecoration(
        image: new DecorationImage(
          fit: BoxFit.cover,
          image: new AssetImage('assets/images/drawer_header_background.webp'),
        ),
      ),
    );
  }

  Widget _buildDrawerItem(GameState state) {
    var item = DrawerItems[state];
    return new ListTileTheme(
      selectedColor: item.color,
      style: ListTileStyle.drawer,
      child: new ListTile(
        leading: new Icon(item.icon),
        title: new Text(item.title),
        selected: state == widget._selectedState,
        onTap: () {
          widget.onSelectDrawerItem(state);
          Navigator.of(context).pop();
        },
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class FadeInImageProvider implements ImageProvider {
  final String _url;
  ImageProvider _provider;

  FadeInImageProvider(this._url) {
    if (_url != null) {
      final image = new FadeInImage.memoryNetwork(
        placeholder: kTransparentImage,
        image: _url,
      );
      _provider = image.image;
    } else {
      _provider = new Image.memory(kTransparentImage).image;
    }
  }

  @override
  ImageStreamCompleter load(key) {
    return _provider.load(key);
  }

  @override
  Future obtainKey(ImageConfiguration configuration) {
    return _provider.obtainKey(configuration);
  }

  @override
  ImageStream resolve(ImageConfiguration configuration) {
    return _provider.resolve(configuration);
  }
}

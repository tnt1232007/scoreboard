import 'package:flutter/material.dart';

import '../../common/constants.dart';

class CircleTextAvatar extends StatelessWidget {
  final String text;
  final int color;

  CircleTextAvatar({this.text, this.color});

  @override
  Widget build(BuildContext context) {
    return new CircleAvatar(
      child: new Text(
        text,
        style: new TextStyle(
          fontWeight: FontWeight.w300,
          color: Colors.white,
        ),
      ),
      backgroundColor: new Color(color),
      radius: 25.0,
    );
  }
}

class CircleIconAvatar extends StatelessWidget {
  final IconData icon;
  final int color;

  CircleIconAvatar({this.icon, this.color});

  @override
  Widget build(BuildContext context) {
    return new CircleAvatar(
      child: new Icon(
        icon,
        color: Colors.white,
      ),
      backgroundColor: new Color(color ?? Constants.deactiveColor.value),
      radius: 25.0,
    );
  }
}

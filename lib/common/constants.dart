import 'package:flutter/material.dart';

class Constants {
  static const PositiveColor = Colors.green;
  static const NegativeColor = Colors.red;

  static const ActiveColor = Colors.blue;
  static const ArchiveColor = Colors.orange;
  static const DeleteColor = Colors.red;

  static final deactiveColor = Colors.grey.shade600;
  static final selectedColor = Colors.grey.withAlpha(120);

  static final plusMinus = new String.fromCharCodes(new Runes('\u00B1'));
  static final plus = new String.fromCharCodes(new Runes('\u002B'));
  static final minus = new String.fromCharCodes(new Runes('\u2212'));
}

import 'dart:math';

class Randomizer {
  static Random seed = new Random();

  static int _number(int min, int max) => seed.nextInt(max + 1 - min) + min;

  /// Get a random int (inclusive)
  static int $int({int min = 0, int max = 1}) {
    return _number(min, max);
  }

  /// Get a random even number (inclusive)
  static int even({int min = 0, int max = 1}) {
    var result = 0;
    do {
      result = _number(min, max);
    } while (result % 2 == 1);
    return result;
  }

  /// Get a random odd number (inclusive)
  static int odd({int min = 0, int max = 1}) {
    int result = 0;
    do {
      result = _number(min, max);
    } while (result % 2 == 0);
    return result;
  }

  /// Get a random sequence of digits (inclusive)
  static List<int> digits(int count, [int minDigit = 0, int maxDigit = 9]) {
    if (maxDigit > 9 || maxDigit < 0) throw new Exception("max digit can't be lager than 9 or smaller than 0");
    if (minDigit > 9 || minDigit < 0) throw new Exception("min digit can't be lager than 9 or smaller than 0");

    var digits = new List<int>(count);
    for (var i = 0; i < count; i++) digits[i] = _number(minDigit, maxDigit);
    return digits;
  }

  /// Get a random double (inclusive)
  static double $double({num min = 0.0, num max = 1.0}) {
    if (min == 0.0 && max == 1.0) return seed.nextDouble();
    return seed.nextDouble() * (max - min) + min;
  }

  static int char({bool useUpperChars = true, bool useLowerChars = true}) {
    var upperChar = _number(65, 90);
    var lowerChar = _number(97, 122);
    if (useUpperChars && useLowerChars)
      return seed.nextBool() ? upperChar : lowerChar;
    else if (useUpperChars)
      return upperChar;
    else if (useLowerChars)
      return lowerChar;
    else
      throw new Exception("must use either uppercase or lowercase or both");
  }

  /// Generate a random chars
  static List<int> chars({int minCount = 4, int maxCount = 12, bool useUpperChars = true, bool useLowerChars = true}) {
    var count = _number(minCount, maxCount);
    var result = new List<int>(count);
    for (var i = 0; i < count; i++)
      result.add(char());
    return result;
  }

  /// Get a string of characters of a random length (default from 40 to 80)
  static String $String({int minLength = 40, int maxLength = 80}) {
    return new String.fromCharCodes(chars(minCount: minLength, maxCount: maxLength));
  }
  
  /// Get a random array element.
  static T pick<T>(List<T> list)
  {
      var i = _number(0, list.length - 1);
      return list[i];
  }
}
